import { useCallback } from 'react'

export const useMEssage = () => {
    return useCallback(text => {
        if (window.M && text) {
            window.M.toast({ html: text })
        }
    }, [])
}