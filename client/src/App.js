import React from "react";
import { useRoutes } from "./routes";
import { useAuth } from "./hooks/auth.hook";
import { BrowserRouter as Router } from "react-router-dom";
import { AuthContext } from "./context/AuthContext.js";
import { Navbar } from './components/Navbar.js'
import { Loader } from './components/Loader.js'
import "materialize-css";


function App() {
  const { login, logout, token, userId, ready } = useAuth();
  const isAuthenticated = !!token
  const routes = useRoutes(isAuthenticated);

  if(!ready) {
    return <Loader />
  }

  return (
    <AuthContext.Provider value={{
      login, logout, token, userId, isAuthenticated
    }}>
      <Router>
        { isAuthenticated && <Navbar /> }
        <div className="container">{routes}</div>
      </Router>
    </AuthContext.Provider>
  );
}

export default App;
